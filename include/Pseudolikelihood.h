/*************************************************************************
Copyright (c) Duccio Malinverni (EPFL)
>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
*************************************************************************/

#ifndef DEF_PSEUDOLIKELIHOOD
#define DEF_PSEUDOLIKELIHOOD

#include "Utils.h"
#include "optimization.h"
#include "stdafx.h"
using namespace alglib;

double g_dg_r(const unsigned int r, const double* wr, double* gradr,const Parameters& parameters);

#endif
