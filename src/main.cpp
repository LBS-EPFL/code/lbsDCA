/*************************************************************************                                                            
Copyright (c) Duccio Malinverni (EPFL)
>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.
                                                                                                                                       
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
*************************************************************************/

#include "FastaHandler.h"
#include "Optimizer.h"
#include "InputHandler.h"

using namespace std;

int main(int argc, char** argv){


  //Parse parameters and load fasta file to MSA object
  Parameters parameters=parseInput(argc,argv);
  
 //Extract parameters from MSA and build parameters structure
  unsigned int q=parameters.q;
  unsigned int N=parameters.N;
  unsigned int nParams=q*N + q*q*N*N;

  //Setup full parameter vector and run optimization for each r independently
  cout<<"Initializing potts parameters...";
  double* w= new double[nParams];  
  loadInitPottsParams(w,parameters.initParamsFile,nParams);
  cout<<"done"<<endl;
  
  //Display simulation setup
  cout<<"Loaded Model: N="<<parameters.N<<" , B="<<parameters.B<<" , Beff="<<parameters.Beff<<" , q="<<q
      <<" , lambdaH="<<parameters.lambdaH<<" , lambdaJ="<<parameters.lambdaJ<<endl;
  cout<<"Performing optimization with "<<parameters.nThreads<<" threads."<<endl;

//Setup timer
  double timer=omp_get_wtime();  
  omp_set_num_threads(parameters.nThreads); 
#pragma omp parallel for schedule(static) 
  for(unsigned int r=0;r<N;r++ ){
    printf("Starting r= %d\n",r);
    optimizeWr(r,&(w[Nr(r,q,N)]),parameters);
  }

  //Shift couplings and fields in the Ising gauge
  double* Jising=new double[q*q*N*(N-1)/2];
  double* hIsing=new double[q*N];
  shiftCouplingsToIsingGauge(w,Jising,hIsing,q,N);

  //Compute Scores and apply APC
  double* S=new double[N*(N-1)/2];
  for (unsigned int i=0;i<N-1;i++)
    for(unsigned int j=i+1;j<N;j++){
      if(parameters.countGaps)
	S[J_up(i,j,N)]=frobeniusNorm(&Jising[J_up(i,j,N)*q*q],q);
      else
	S[J_up(i,j,N)]=frobeniusNormWithoutGaps(&Jising[J_up(i,j,N)*q*q],q);
    }
  double* S_apc=new double[N*(N-1)/2];
  apcTransform(S,S_apc,N);
  
  //Write output
  ofstream out((parameters.outFile+".dat").c_str());
  ofstream outRaw((parameters.outFile+"_raw.dat").c_str());
  for(unsigned int i=0;i<N*(N-1)/2;i++){
    out<<S_apc[i]<<endl;
    outRaw<<S[i]<<endl;
  }
  out.close();

  if(parameters.saveFullCouplings){
    ofstream binOut((parameters.outFile+"_fullParams.dat").c_str(),ios::out|ios::binary);
    binOut.write((char*)w,sizeof(double)*nParams);
    binOut.close();
  }
  if(parameters.saveCouplings){
    ofstream binOut((parameters.outFile+"_isingJs.dat").c_str(),ios::out|ios::binary);
    binOut.write((char*)Jising,sizeof(double)*q*q*N*(N-1)/2);
    binOut.close();
    binOut.open((parameters.outFile+"_isingHs.dat").c_str(),ios::out|ios::binary);
    binOut.write((char*)hIsing,sizeof(double)*q*N);
    binOut.close();
  }

  delete[] w;
  delete[] Jising;
  delete[] hIsing;
  delete[] S;
  delete[] S_apc;
  cout<<"PLM DCA finished in : "<<omp_get_wtime()-timer<<" seconds."<<endl;
  return 0;
}
