/*************************************************************************                                                            
Copyright (c) Duccio Malinverni (EPFL)
>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.
                                                                                                                                       
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
*************************************************************************/

#include "InputHandler.h"
using namespace std;

Parameters parseInput(int argc, char** argv){

  //If help option passed display usage:
  if(argc==1 || !string(argv[1]).compare("-h") || !string(argv[1]).compare("--help")){
    cout<<"Usage: lbsDCA [options]"<<endl;
    cout<<"       -f    : Fasta file "<<endl;
    cout<<"       -in   : Arbitratry input file"<<endl;
    cout<<"       -o    : Output file [Default: output.dat]"<<endl;
    cout<<"       -lh   : Lambda H [default 0.01]"<<endl;
    cout<<"       -lj   : Lambda J [default 0.01]"<<endl;
    cout<<"       -q    : Number of states q [default 21]"<<endl;
    cout<<"       -t    : Number of threads [default 1]"<<endl;
    cout<<"       -s    : Shuffle the input matrix"<<endl;
    cout<<"       -g    : Consider the gaps coupling when performing Frobenius norm [default no]"<<endl; 
    cout<<"       -full : Save complete couplings, i.e. N*N*q*q raw parameters, withoug APC"<<endl;
    cout<<"       -jfull: Save complete couplings in Ising gauge, i.e. N(N-1)*q*q/2 parameters"<<endl;
    cout<<"       -w    : Load weights for sequences from file [default none]"<<endl;
    cout<<"       -wid  : Compute weights for sequences, based on sequence identity [default none]"<<endl;
    cout<<"       -x    : Load initial parameters of the potts model [default none]"<<endl;
    cout<<"       -max  : Maximum number of symbols to keep for each position [all]"<<endl;
    exit(0);
}

  Parameters params;
  //Define default parameters
  params.lambdaH=0.01;
  params.lambdaJ=0.005;
  params.q=21;
  params.outFile="output.dat";
  params.nThreads=1;
  params.msa=NULL;
  params.saveFullCouplings=false;
  params.saveCouplings=false;
  params.countGaps=false;
  bool shuffle=false;
  int maxSymbols=-1;
  string weightsFile="none";
  double weightsId=-1;
  params.initParamsFile="none";
  //Parse input arguments
  for(int i=0;i<argc;i++){
    if(!string(argv[i]).compare("-o")){
	params.outFile=string(argv[i+1]);
	i++;
      }
    if(!string(argv[i]).compare("-t")){
	params.nThreads=atoi(argv[i+1]);
	i++;
      }
    if(!string(argv[i]).compare("-lh")){
	params.lambdaH=atof(argv[i+1]);
	i++;
      }
    if(!string(argv[i]).compare("-lj")){
	params.lambdaJ=atof(argv[i+1]);
	i++;
      }
    if(!string(argv[i]).compare("-q")){
	params.q=atoi(argv[i+1]);
	i++;
      }
    if(!string(argv[i]).compare("-s")){
      shuffle=true;
      }
    if(!string(argv[i]).compare("-max")){
      maxSymbols=atoi(argv[i+1]);
      i++;
    }
    if(!string(argv[i]).compare("-f")){
      cout<<"Reading fasta file..."<<flush;
      params.msa=readFasta(string(argv[i+1]),params.N,params.B);
      params.Beff=params.B;
      cout<<"done."<<endl;  
      i++;
      }
    if(!string(argv[i]).compare("-full")){
      params.saveFullCouplings=true;
      }
    if(!string(argv[i]).compare("-jfull")){
      params.saveCouplings=true;
      }
    if(!string(argv[i]).compare("-g")){
      params.countGaps=true;
      }
    if(!string(argv[i]).compare("-in")){
      cout<<"Reading ascii file..."<<endl;
      params.msa=readASCII(string(argv[i+1]),params.N,params.B);
      params.Beff=params.B;
      cout<<"done."<<endl;  
      i++;
      }
    if(!string(argv[i]).compare("-w")){
      weightsFile=string(argv[i+1]);
      i++;
    }
    if(!string(argv[i]).compare("-wid")){
      weightsId=atof(argv[i+1]);
      i++;
    }
    if(!string(argv[i]).compare("-x")){
      params.initParamsFile=string(argv[i+1]);
      i++;
    }
  }

  if(params.msa==NULL){
    cout<<"Error: No input file specified or unable to read input file."<<endl;
    exit(1);
  }
  
  params.seqWeights=new double[params.B];
  if(weightsFile.compare("none"))
    loadSeqWeights(params.seqWeights,weightsFile,params.Beff);
  else
    fill_n(params.seqWeights,params.B,1.0);
  
  if(shuffle)
    shuffleDataMatrix(params.msa,params.N,params.B);

  if(weightsId>0){
    cout<<"Computing sequence weights with max "<<weightsId<<" sequence identity..."<<flush;
    computeSeqIdWeights(params.seqWeights,params.msa,params.N,params.B,params.Beff,weightsId);    
    cout<<"done."<<endl;
  }  
  if(maxSymbols!=-1)
    pruneMSA(params.msa,params.N,params.B,maxSymbols,params.q);
  params.lambdaH*=params.Beff;
  params.lambdaJ*=params.Beff;
  return params;
}

MSA readASCII(std::string inputFile,unsigned int& N, unsigned int& B){
  ifstream inStream(inputFile.c_str());
  string line;
  unsigned int Nt=0,value=0;
  N=0;
  getline(inStream,line);
  istringstream lineStream(line);
  //Determine number of entries
  while(!lineStream.eof()){
    N++;
    lineStream>>value;
  }
  inStream.close();
  inStream.open(inputFile.c_str());
  Sequence tmp(0);
  while(!inStream.eof()){
    inStream>>value;
    tmp.push_back(value);
    Nt++;
  }
  inStream.close();
  
  B=Nt/N;
  
  MSA msa=new unsigned int[B*N];
  for(unsigned int i=0;i<Nt;i++)
    msa[i]=tmp[i];
  return msa;
}

void shuffleDataMatrix(MSA msa, const unsigned int N, const unsigned int B){
  //Reload data in std. vector, so to use the built-in shuffle function
  cout<<"Performing shuffling of the data matrix...";
  Sequence tmp;
  std::srand(unsigned(std::time(0)));

  for(unsigned int i=0;i<N;i++){
    tmp=Sequence(0);
    for(unsigned int b=0;b<B;b++)
      tmp.push_back(msa[b*N+i]);
    std::random_shuffle(tmp.begin(),tmp.end());
    for(unsigned int b=0;b<B;b++)
      msa[b*N+i]=tmp[b];
  }
  cout<<"done"<<endl;
}

void loadSeqWeights(double* weights,string weightsFile,double& Beff){
  ifstream inFile(weightsFile.c_str());
  unsigned int i=0;
  Beff=0;
  while(!inFile.eof()){
    inFile>>weights[i];
    Beff+=weights[i];
    i++;
  }
  inFile.close();
}

void loadInitPottsParams(double* w,std::string paramsFile,unsigned int nParams){
  if(!(paramsFile.compare("none")))
    std::fill_n(w,nParams,0.);
  else{
    ifstream inFile(paramsFile.c_str());
    unsigned int i=0;
    while(!inFile.eof()){
      inFile>>w[i];
      i++;
    }
    inFile.close();
  }
}

void computeSeqIdWeights(double* weights, MSA msa,unsigned int N, unsigned int B, double& Beff, double minId){
  unsigned int idCount=0;
  Beff=0;
  std::fill_n(weights,B,1.);
  for(unsigned int b=0;b<B-1;b++){
    for(unsigned int bp=b+1;bp<B;bp++){
      unsigned int idCount=0;
      for(unsigned int i=0;i<N;i++)
	idCount+=(msa[b*N+i]==msa[bp*N+i]);
      weights[b]+=((double(idCount)/N)>=minId);
      weights[bp]+=((double(idCount)/N)>=minId);
    }
  }
  for(unsigned int b=0;b<B;b++){
    weights[b]=1/weights[b];
    Beff+=weights[b];
  }
}

unsigned int pruneMSA(MSA& msa, const unsigned int N, 
		      const unsigned int B, const unsigned int maxSymbols, unsigned int& q){
  unsigned int* fi=new unsigned int[N*q];
  fill_n(fi,N*q,0);
  for(unsigned int b=0;b<B;b++)
    for(unsigned int i=0;i<N;i++)
      fi[i*q+msa[b*N+i]]++;  

  MSA prunedMSA= new unsigned int[B*N];
  std::fill_n(prunedMSA,B*N,maxSymbols);
  std::vector<unsigned int> c(q,0);
  std::vector<unsigned int> indexes;
  for(unsigned int i=0;i<N;i++){
    c.assign(&(fi[i*q]),&(fi[i*q+q]));
    indexes=getSortIndexes(c);
    std::stable_sort(c.begin(),c.end(),std::greater<unsigned int>());
    for(unsigned int b=0;b<B;b++)
      for(unsigned int p=0;p<maxSymbols;p++){
	if(msa[b*N+i]==indexes[p])
	  prunedMSA[b*N+i]=p;
      }
  }

  delete[] msa;
  msa=prunedMSA;
  q=maxSymbols+1;
}
