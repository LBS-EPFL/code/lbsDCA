/*************************************************************************                                                            
Copyright (c) Duccio Malinverni (EPFL)
>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.
                                                                                                                                       
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
*************************************************************************/

#include "FastaHandler.h"

/* Reads a fasta file, input as string, and return an unsigned int array containing the amino acids sequences.
The array is accessed as msa[b][i]= msa[b*N+i]. Also returns the dimensions B (number of sequences) 
and N (number of nodes) of the msa
*/
MSA readFasta(std::string fastaFile,unsigned int &N, unsigned int &B){

  std::ifstream fastaStream(fastaFile.c_str());
  MSA msaFull;
  std::vector<Sequence> msa;
  std::string line;
  unsigned int b=-1;

  while(!fastaStream.eof()){
    fastaStream >> line;
    if(line[0]=='>'){
      msa.push_back(Sequence(0));
      b++;
      std::getline(fastaStream,line);
    }
   else
      for(unsigned int i=0;i<line.size();i++)
	msa[b].push_back(AAtoNumber(line[i]));
  }
  fastaStream.close();
  
  B=msa.size();
  N=msa[0].size();
  
  msaFull=new unsigned int[B*N];
  for(unsigned int i=0;i<B;i++)
    for(unsigned int j=0;j<N;j++)
      msaFull[i*N+j]=msa[i][j];
  return msaFull;
}

/* Converts amino acid characters to numerical form, using the alphabetical ordering*/
unsigned int AAtoNumber(char AA){

  switch(AA){
    case '-': return 0;
    case 'A': return 1; 
    case 'C': return 2;
    case 'D': return 3;
    case 'E': return 4;
    case 'F': return 5;
    case 'G': return 6;
    case 'H': return 7;
    case 'I': return 8;
    case 'K': return 9;
    case 'L': return 10;
    case 'M': return 11;
    case 'N': return 12;
    case 'P': return 13;
    case 'Q': return 14;
    case 'R': return 15;
    case 'S': return 16;
    case 'T': return 17;
    case 'V': return 18;
    case 'W': return 19;
    case 'Y': return 20;
  }
  return 0;
}
