/*************************************************************************                                                            
Copyright (c) Duccio Malinverni (EPFL)
>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.
                                                                                                                                       
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
*************************************************************************/

#include "Optimizer.h"


/*Interface to the alglib optimization library. Basically, this calls the function and gradient evaluation, 
and interfaces the native double arrays to the alglib real_1d_array objects
*/
void alglibInterface(const real_1d_array &x, double &func, real_1d_array &grad, void* ptr){
  optimParameters* parms=(optimParameters*) ptr;
  func= g_dg_r(parms->r,x.getcontent(),grad.getcontent(),parms->params);
}

/*Main optimization function. Performs a Pseudolikelihood optimization for a given node r. 
Sets up the optimization routines, extracts the parameters, runs the optimization 
and returns the optimized values as native arrays.
 */
void optimizeWr(const unsigned int r, double* w,const  Parameters& parameters){
  
  unsigned int q=parameters.q;
  unsigned int N=parameters.N;
  
  //Setup structure containing the parameters to be passed to the optimizer
  optimParameters parms;
  parms.r=r;
  parms.params=parameters;
  //Setup the vector containing the weights to be optimized
  real_1d_array x;
  x.setcontent(q+q*q*N, w);
  //Setup the alglib optimizer objects and define the optimization options
  minlbfgsstate state;
  minlbfgsreport rep;
  ae_int_t maxits=300; 
  double epsg = 0;
  double epsf = 1e-5;
  double epsx = 0;
  minlbfgscreate(15,x, state);
  minlbfgssetcond(state, epsg, epsf, epsx, maxits);
  minlbfgsoptimize(state, alglibInterface,NULL,&parms);
  minlbfgsresults(state, x, rep);
  //Copty the optimized weights to the standard double pointer for output
  for(unsigned int i=0;i<q+q*q*N;i++)
    w[i]=x[i];
}
