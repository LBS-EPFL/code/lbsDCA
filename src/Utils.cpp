/*************************************************************************                                                            
Copyright (c) Duccio Malinverni (EPFL)
>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.
                                                                                                                                       
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses
>>> END OF LICENSE >>>
*************************************************************************/

#include "Utils.h"

//Performs the average of the coupling matrix along direction i/j, return a 1-d vector containing the averages
void meanJ_i(double* w,unsigned int i, unsigned int j,unsigned int q,unsigned int N,double* meanJi,bool direction){
  std::fill_n(meanJi,q,0.);
  for(unsigned int k=0;k<q;k++){
    for(unsigned int l=0;l<q;l++)
      if(direction)
	meanJi[k]+=w[Nr(i,q,N)+J(j,k,l,q)];
      else
	meanJi[k]+=w[Nr(i,q,N)+J(j,l,k,q)];
    meanJi[k]/=q;
  }
}

//Performs the complete average over all J_ij of the coupling matrix
double meanJ_ij(double* meanJi, unsigned int q){
  double mean=0;
  for(unsigned int i=0;i<q;i++)
    mean+=meanJi[i];
  return mean/q;

}

/*Shifts the coupling matrix J_ij(A,B) in the Ising gauge, defined by:
J_ij(k,l)_ising = J_ij(k,l) - J_ij(#,l) - J_ij(k,#) + J_ij(#,#)
and the fields to
h_i(k)_ising = h_i(k) + sum_j[ J_ij(k,#) ] + sum_j[ J_ji(#,k)]
*/
void shiftCouplingsToIsingGauge(double* w, double* Jising,double* hIsing, unsigned int q, unsigned int N){
  double* mi1=new double[q];
  double* mj1=new double[q];
  double* mi2=new double[q];
  double* mj2=new double[q];
  double M1=0,M2=0;
  std::fill_n(hIsing,N*q,0.);

  for(unsigned int i=0;i<N;i++){
    for(unsigned int k=0;k<q;k++)
      hIsing[q*i+k] += w[Nr(i,q,N)+k];
    for(unsigned int j=i+1;j<N;j++){
      meanJ_i(w,i,j,q,N,mi1,0);
      meanJ_i(w,i,j,q,N,mj1,1);
      meanJ_i(w,j,i,q,N,mi2,0);
      meanJ_i(w,j,i,q,N,mj2,1);
      M1=meanJ_ij(mi1,q);
      M2=meanJ_ij(mi2,q);
      for(unsigned int k=0;k<q;k++){
	hIsing[q*i+k]+=mj1[k];
	hIsing[q*j+k]+=mj2[k];
	for(unsigned int l=0;l<q;l++)
	  Jising[J_up(i,j,N)*q*q + q*k + l]=0.5*(w[Nr(i,q,N)+J(j,k,l,q)]-mi1[l]-mj1[k] + M1 + 
						 w[Nr(j,q,N)+J(i,l,k,q)]-mi2[k]-mj2[l] + M2);
      }
    }
  }
  delete[] mi1;
  delete[] mi2;
  delete[] mj1;
  delete[] mj2;
}

/* Applies the average product correction to the matrix S, stored in half triangular form:
S_ij_APC = S_ij -S_i#S_#j/S_##
 */
void apcTransform(double* S, double* S_apc,unsigned int N){

  double Smean=0;
  double* Si = new double[N];
  double* Sj = new double[N];
  std::fill_n(Si,N,0.);
  std::fill_n(Sj,N,0.);

  for(unsigned int k=0;k<N;k++)
    for(unsigned int l=0;l<N;l++){
      if(k==l)
	continue;
      Si[k]+=S[J_up(k,l,N)];
      Smean+=S[J_up(k,l,N)];
    }
  
  for(unsigned int i=0;i<N-1;i++)
    for(unsigned int j=i+1;j<N;j++)
      S_apc[J_up(i,j,N)]=S[J_up(i,j,N)] - Si[i]*Si[j]/Smean;
  
  delete[] Si;
  delete[] Sj;
}

std::vector<unsigned int> getSortIndexes(std::vector<unsigned int> const& values) {
  std::vector<unsigned int> indices(values.size());
  std::iota(indices.begin(), indices.end(), static_cast<unsigned int>(0));
  std::stable_sort(indices.begin(), indices.end(),
	    [&](unsigned int a, unsigned int b) { return values[a] > values[b]; });
  return indices;
}
